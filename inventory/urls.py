from django.urls import path
from .views import Purchase_order_View, Bill_Of_Stock_View

urlpatterns = [
    path('purchase/order/', Purchase_order_View, name="purchase"),
    path('bill/of/stock/', Bill_Of_Stock_View, name="billofstock"),
]