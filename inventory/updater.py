from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from api_v1.inventory import StockSerializer
from .models import BillOfStockItem, StockComputation

def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(StockSerializer.update_forecast, 'interval', minutes=5)
    scheduler.start()

