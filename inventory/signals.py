from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from inventory.models import StockComputation, BillOfStockItem

# @receiver(post_save, sender=BillOfStockItem)
def stock_auto_save(instance, **kwargs):
    item = instance.item
    uom = instance.uom
    unit_price = instance.unit_price
    try:
        print("__________try block called_______")
        # itms = StockComputation.objects.filter(item_name=item).last()
        s1 = StockComputation.objects.get(item_name=item, store_units=instance.send_to, created_at=datetime.today())
        s1.received_stock += instance.received_quantity
        s1.unit_price = instance.unit_price
        # s1.theoretical_QoH = itms.final_closing_stock + instance.received_quantity + int(s1.transferred_stock)
        # s1.final_closing_stock = s1.theoretical_QoH
        weighs_price = ((stock.unit_price * stock.received_stock) + (instance.unit_price * instance.received_quantity)) / (stock.received_stock + instance.received_quantity)
        print(weighs_price)
        s1.save()

    except:
        print("________except block called______________")
        stock_quantity = instance.received_quantity
        store_units = instance.send_to
        # itm = StockComputation.objects.filter(item_name=item).last()
        stock = StockComputation.objects.create(store_units=store_units,item_name=item, received_stock=stock_quantity, uom=uom, unit_price=unit_price, final_closing_stock=instance.received_quantity)
        # stock.opening_stock = itm.final_closing_stock
        # stock.theoretical_QoH = itm.final_closing_stock + int(stock.opening_stock) + int(stock_quantity) + int(stock.transferred_stock) + int(stock.ordered_stock) - int(stock.expired_stock) - int(stock.sale_stock) - int(stock.complimentory_stock)
        # stock.final_closing_stock = stock_quantity
        stock.save()


post_save.connect(stock_auto_save, sender=BillOfStockItem)


"""signal to auto save bill of stock item in stock without dublicating item"""
def auto_save_stock(instance, **kwargs):
    item = instance.item
    uom = instance.uom
    unit_price = instance.unit_price
    stock_quantity = instance.received_quantity
    weigh_price = instance.unit_price
    try:
        check_item = Stock.objects.get(item=item)
        if check_item is not None:
            print("try, _____________________")
            atm = Stock.objects.filter(item=instance.item).last().unit_price
            print(atm,"atm _______________")
            atm1 = instance.unit_price
            print(atm1, " atm1________________")
            atm3 = instance.received_quantity
            print(atm3, "atm3_________________")
            s1 = Stock.objects.get(item=instance.item)
            s1.stock_quantity += instance.received_quantity
            atm4 = s1.stock_quantity
            print(atm4, "atm4____________________")
            s1.unit_price = instance.unit_price
            s1.weigh_price = ((atm * atm4) + (atm1 * atm3)) / (atm3 + atm4)
            s1.save()
    except:
        print("except_____________________")
        s2 = Stock.objects.create(item=item, uom= uom, unit_price= unit_price, stock_quantity = stock_quantity, weigh_price=weigh_price)
        s2.save()
    
pre_save.connect(auto_save_stock, sender=BillOfStockItem)