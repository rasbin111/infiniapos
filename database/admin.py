from django.contrib import admin
from django import forms
from database.models import Customer, CreditHistory, CalculateTotalcredit, Vendor, ContactPerson
from django.contrib import admin
from database.models import (
    Customer, CreditHistory, Credit, Floor,
    SeatingType,
    CompanyDetail, Branch, Reward,
)


admin.site.register([ContactPerson, ])
admin.site.register(Customer)


class ContactPersonInline(admin.TabularInline):
    model = ContactPerson


class VendorAdmin(admin.ModelAdmin):
    inlines = [ContactPersonInline]

admin.site.register(Vendor, VendorAdmin)

class CreditHistoryHelperInline(admin.TabularInline):
    model = Credit


class CreditHistoryAdmin(admin.ModelAdmin):
    inlines = [
        CreditHistoryHelperInline
    ]


admin.site.register(CreditHistory, CreditHistoryAdmin)


class CreditHistoryInline(admin.TabularInline):
    model = CreditHistory


class CustomerAdmin(admin.ModelAdmin):
    inlines = [
        CreditHistoryInline,
    ]


admin.site.register(Customer, CustomerAdmin)
admin.site.register([Floor, SeatingType, CompanyDetail, Branch, Reward, Credit])
